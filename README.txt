Culinary Antidote as a web application based on REST architecture.
Logic layer of web application with culinary recipes created in JAVA.
The application presents searching, creating, editing and deleting functionalities.
Culinary antidote application uses frameworks and technologies:
Spring (Boot, Security, Data), Hibernate, PostgreSQL, Maven)
