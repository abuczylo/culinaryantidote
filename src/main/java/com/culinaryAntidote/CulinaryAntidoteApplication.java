package com.culinaryAntidote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CulinaryAntidoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CulinaryAntidoteApplication.class, args);
	}

}
