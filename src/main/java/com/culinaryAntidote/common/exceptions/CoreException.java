package com.culinaryAntidote.common.exceptions;

import lombok.Data;

@Data
public abstract class CoreException extends RuntimeException {

    private final String errorCode;

    protected CoreException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
