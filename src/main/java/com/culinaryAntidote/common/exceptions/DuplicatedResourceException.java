package com.culinaryAntidote.common.exceptions;

public class DuplicatedResourceException extends CoreException {

    public DuplicatedResourceException(String errorCode, String message) {
        super(errorCode, message);
    }
}
