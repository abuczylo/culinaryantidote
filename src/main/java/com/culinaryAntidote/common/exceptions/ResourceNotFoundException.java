package com.culinaryAntidote.common.exceptions;

public class ResourceNotFoundException extends CoreException {

    public ResourceNotFoundException(String errorCode, String message) {
        super(errorCode, message);
    }
}
