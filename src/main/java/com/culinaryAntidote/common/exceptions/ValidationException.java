package com.culinaryAntidote.common.exceptions;

public class ValidationException extends CoreException {

    public ValidationException(String errorCode, String message) {
        super(errorCode, message);
    }
}
