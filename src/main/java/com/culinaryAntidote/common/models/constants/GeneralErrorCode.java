package com.culinaryAntidote.common.models.constants;

public class GeneralErrorCode {

    public static final String resourceNotFoundErrorCode = "ResourceNotFound";
    public static final String internalServerErrorCode = "InternalServerError";
    public static final String duplicatedResourceErrorCode = "DuplicatedResource";
    public static final String validationErrorCode = "ValidationFailed";
    public static final String idMismatchErrorCode = "IdMismatch";
    public static final String invalidCredentialsErrorCode = "InvalidCredentials";
}
