package com.culinaryAntidote.common.models.constants;

public class GeneralErrorMessage {

    public static final String resourceNotFoundErrorMessage = "Resource not found.";
    public static final String internalServerErrorMessage = "Internal server error.";
    public static final String duplicatedResourceErrorMessage = "Resource already exist.";
    public static final String validationErrorMessage = "Validation failed.";
    public static final String idMismatchErrorMessage = "Path id value does not match request body id value.";
    public static final String invalidCredentialsErrorMessage = "Invalid email or password.";
}
