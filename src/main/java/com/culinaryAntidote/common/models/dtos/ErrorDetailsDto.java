package com.culinaryAntidote.common.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorDetailsDto {
    private String errorCode;
    private String errorMessage;
}
