package com.culinaryAntidote.common.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorDto {

    private int status;
    private String path;
    private String errorCode;
    private String errorMessage;
    private List<ErrorDetailsDto> details = new ArrayList<>();
}
