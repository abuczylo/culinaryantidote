package com.culinaryAntidote.core.exceptionHandlers;

import com.culinaryAntidote.common.exceptions.CoreException;
import com.culinaryAntidote.common.exceptions.DuplicatedResourceException;
import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.common.models.dtos.ErrorDetailsDto;
import com.culinaryAntidote.common.models.dtos.ErrorDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.Array;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { ResourceNotFoundException.class, DuplicatedResourceException.class,
            ValidationException.class })
    protected ResponseEntity<Object> handleCoreException(CoreException ex, WebRequest request
            , HttpServletRequest serverRequest) {

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String errorCode = GeneralErrorCode.internalServerErrorCode;
        String errorMessage = GeneralErrorMessage.internalServerErrorMessage;
        List<ErrorDetailsDto> details = new ArrayList<>(Arrays.asList(new ErrorDetailsDto(ex.getErrorCode(),
                ex.getMessage())));

        if (ex instanceof ResourceNotFoundException) {
            httpStatus = HttpStatus.NOT_FOUND;
            errorCode = GeneralErrorCode.resourceNotFoundErrorCode;
            errorMessage = GeneralErrorMessage.resourceNotFoundErrorMessage;
        } else if(ex instanceof DuplicatedResourceException) {
            httpStatus = HttpStatus.CONFLICT;
            errorCode = GeneralErrorCode.duplicatedResourceErrorCode;
            errorMessage = GeneralErrorMessage.duplicatedResourceErrorMessage;
        } else if(ex instanceof ValidationException) {
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            errorCode = GeneralErrorCode.validationErrorCode;
            errorMessage = GeneralErrorMessage.validationErrorMessage;
        }

        ErrorDto errorDto = new ErrorDto(httpStatus.value(), serverRequest.getRequestURI(), errorCode, errorMessage,
                details);
        return handleExceptionInternal(ex, errorDto, new HttpHeaders(), httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        List<ErrorDetailsDto> details =
                ex.getBindingResult().getAllErrors().stream().map(error -> new ErrorDetailsDto(error.getCode(),
                        error.getDefaultMessage())).collect(Collectors.toList());

        ErrorDto errorDto = new ErrorDto(HttpStatus.UNPROCESSABLE_ENTITY.value(), request.getContextPath(),
                GeneralErrorCode.validationErrorCode, GeneralErrorMessage.validationErrorMessage,
                details);
        return handleExceptionInternal(ex, errorDto, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
    }
}
