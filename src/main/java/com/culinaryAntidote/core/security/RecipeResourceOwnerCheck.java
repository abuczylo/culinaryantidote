package com.culinaryAntidote.core.security;

import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.recipes.services.RecipeErrorCode;
import com.culinaryAntidote.recipes.services.RecipeErrorMessage;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class RecipeResourceOwnerCheck {

    private UserRepository userRepository;
    private RecipeRepository recipeRepository;

    public boolean check(UUID recipeId, String email) {
        List<User> users = userRepository.findAllByEmail(email);
        User user = users.get(0);
        Recipe recipe =
                recipeRepository.findById(recipeId)
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeErrorCode.recipeNotFoundErrorCode,
                                RecipeErrorMessage.recipeNotFoundErrorMessage));
        return user.getId().equals(recipe.getUser().getId());
    }
}
