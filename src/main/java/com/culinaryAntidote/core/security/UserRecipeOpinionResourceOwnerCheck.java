package com.culinaryAntidote.core.security;

import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinion;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionRepository;
import com.culinaryAntidote.userRecipeOpinions.services.UserRecipeOpinionErrorCode;
import com.culinaryAntidote.userRecipeOpinions.services.UserRecipeOpinionErrorMessage;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class UserRecipeOpinionResourceOwnerCheck {

    private UserRepository userRepository;
    private UserRecipeOpinionRepository userRecipeOpinionRepository;

    public boolean check(UUID userRecipeOpinionId, String email) {
        List<User> users = userRepository.findAllByEmail(email);
        User user = users.get(0);
        UserRecipeOpinion userRecipeOpinion =
                userRecipeOpinionRepository.findById(userRecipeOpinionId)
                        .orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userRecipeOpinionNotFoundErrorCode,
                                UserRecipeOpinionErrorMessage.userRecipeOpinionNotFoundErrorMessage));

        return user.getId().equals(userRecipeOpinion.getUser().getId());
    }
}
