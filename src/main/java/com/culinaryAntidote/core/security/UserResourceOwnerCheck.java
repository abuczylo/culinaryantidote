package com.culinaryAntidote.core.security;

import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class UserResourceOwnerCheck {

    private UserRepository userRepository;

    public boolean check(UUID userId, String email) {
        List<User> users = userRepository.findAllByEmail(email);
        User user = users.get(0);
        return user.getId().equals(userId);
    }
}
