package com.culinaryAntidote.recipeCategories.persistence;

import com.culinaryAntidote.recipes.persistance.Recipe;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(schema="public", name="recipe_categories")
public class RecipeCategory {

    @Id
    @GeneratedValue(generator ="UUID")
    @GenericGenerator(name ="UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name= "id", nullable = false, updatable = false)
    private UUID id;

    @Column(name= "name", nullable = false, unique = true)
    private String name;

    @Column(name= "polish_name", nullable = false, unique = true)
    private String polishName;

    @Column(name= "parentId", nullable = false)
    private UUID parentId;

    @ManyToMany(mappedBy = "recipeCategories")
    private Set<Recipe> recipes = new HashSet<>();
}
