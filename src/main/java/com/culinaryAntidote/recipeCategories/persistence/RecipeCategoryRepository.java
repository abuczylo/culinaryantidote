package com.culinaryAntidote.recipeCategories.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RecipeCategoryRepository extends PagingAndSortingRepository<RecipeCategory, UUID>,
        JpaRepository<RecipeCategory, UUID>, JpaSpecificationExecutor<RecipeCategory> {

    boolean existsByName(String name);
    boolean existsByPolishName(String name);
}
