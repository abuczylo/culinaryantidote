package com.culinaryAntidote.recipeCategories.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecipeCategorySearchCriteria {
    private String name;
    private String polishName;
    private String parentId;
}
