package com.culinaryAntidote.recipeCategories.persistence;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@AllArgsConstructor
public class RecipeCategorySearchSpecification implements Specification<RecipeCategory> {

    private static final String SEARCH_PATTERN = "%%%s%%";
    private final RecipeCategorySearchCriteria searchCriteria;
    private final List<Predicate> predicates = new LinkedList<>();

    @Override
    public Predicate toPredicate(Root<RecipeCategory> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        filterByName(root, criteriaBuilder);
        filterByPolishName(root, criteriaBuilder);
        filterByParentId(root, criteriaBuilder);

        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
    }

    private void filterByName(Root<RecipeCategory> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getName() != null) {
            Predicate predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("name")),
                    format(SEARCH_PATTERN, searchCriteria.getName().toLowerCase())
            );
            predicates.add(predicate);
        }
    }

    private void filterByPolishName(Root<RecipeCategory> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getPolishName() != null) {
            Predicate predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("polishName")),
                    format(SEARCH_PATTERN, searchCriteria.getPolishName().toLowerCase())
            );
            predicates.add(predicate);
        }
    }

    private void filterByParentId(Root<RecipeCategory> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getParentId() != null) {
            Predicate predicate = criteriaBuilder.equal(root.get("parentId"),
                    UUID.fromString(searchCriteria.getParentId()));
            predicates.add(predicate);
        }
    }
}
