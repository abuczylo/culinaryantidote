package com.culinaryAntidote.recipeCategories.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateRecipeCategoryDto {

    @NotBlank(message = "Name is required.")
    private String name;

    @NotBlank(message = "PolishName is required.")
    private String polishName;

    private UUID parentId;
}
