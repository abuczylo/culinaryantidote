package com.culinaryAntidote.recipeCategories.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecipeCategoryDto {
    private UUID id;
    private String name;
    private String polishName;
    private UUID parentId;
}
