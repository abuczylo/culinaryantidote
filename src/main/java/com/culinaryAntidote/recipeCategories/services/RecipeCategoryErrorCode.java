package com.culinaryAntidote.recipeCategories.services;

public class RecipeCategoryErrorCode {

    public static final String recipeCategoryNotFoundErrorCode = "RecipeCategoryNotFound";
    public static final String duplicatedRecipeCategoryNameErrorCode = "DuplicatedRecipeCategoryName";
    public static final String duplicatedRecipeCategoryPolishNameErrorCode = "DuplicatedRecipeCategoryPolishName";
}
