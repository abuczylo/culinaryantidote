package com.culinaryAntidote.recipeCategories.services;

public class RecipeCategoryErrorMessage {

    public static final String recipeCategoryNotFoundErrorMessage = "Recipe category not found.";
    public static final String duplicatedRecipeCategoryErrorMessage = "Recipe category already exist.";
}
