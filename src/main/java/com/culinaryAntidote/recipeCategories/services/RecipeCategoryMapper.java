package com.culinaryAntidote.recipeCategories.services;

import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RecipeCategoryMapper {

    RecipeCategoryDto mapToRecipeCategoryDto(RecipeCategory recipeCategory);
    RecipeCategory mapToRecipeCategory(CreateRecipeCategoryDto createRecipeCategory);
}
