package com.culinaryAntidote.recipeCategories.services;

import com.culinaryAntidote.recipeCategories.persistence.RecipeCategorySearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface RecipeCategoryService {

    Page<RecipeCategoryDto> findAll(Pageable pageable, RecipeCategorySearchCriteria criteria);
    RecipeCategoryDto findById(UUID id);
    RecipeCategoryDto create(CreateRecipeCategoryDto createRecipeCategoryDto);
    RecipeCategoryDto update(UpdateRecipeCategoryDto updateRecipeCategoryDto);
    void delete(UUID id);
}
