package com.culinaryAntidote.recipeCategories.services;

import com.culinaryAntidote.common.exceptions.DuplicatedResourceException;
import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategoryRepository;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategorySearchCriteria;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategorySearchSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class RecipeCategoryServiceImpl implements RecipeCategoryService {

    private final RecipeCategoryRepository recipeCategoryRepository;
    private final RecipeCategoryMapper recipeCategoryMapper;

    @Override
    public Page<RecipeCategoryDto> findAll(Pageable pageable, RecipeCategorySearchCriteria criteria) {
        RecipeCategorySearchSpecification specification = new RecipeCategorySearchSpecification(criteria);
        Page<RecipeCategory> pagedRecipeCategories = recipeCategoryRepository.findAll(specification, pageable);
        return pagedRecipeCategories.map(recipeCategory -> recipeCategoryMapper.mapToRecipeCategoryDto(recipeCategory));
    }

    @Override
    public RecipeCategoryDto findById(UUID id) {
        RecipeCategory recipeCategory =
                recipeCategoryRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeCategoryErrorCode.recipeCategoryNotFoundErrorCode, RecipeCategoryErrorMessage.recipeCategoryNotFoundErrorMessage));
        return recipeCategoryMapper.mapToRecipeCategoryDto(recipeCategory);
    }

    @Override
    public RecipeCategoryDto create(CreateRecipeCategoryDto createRecipeCategoryDto) {

        if(recipeCategoryRepository.existsByName(createRecipeCategoryDto.getName())) {
            throw new DuplicatedResourceException(RecipeCategoryErrorCode.duplicatedRecipeCategoryNameErrorCode,
                    RecipeCategoryErrorMessage.duplicatedRecipeCategoryErrorMessage);
        }
        if(recipeCategoryRepository.existsByPolishName(createRecipeCategoryDto.getPolishName())) {
            throw new DuplicatedResourceException(RecipeCategoryErrorCode.duplicatedRecipeCategoryPolishNameErrorCode,
                    RecipeCategoryErrorMessage.duplicatedRecipeCategoryErrorMessage);
        }

        RecipeCategory recipeCategory = recipeCategoryMapper.mapToRecipeCategory(createRecipeCategoryDto);
        recipeCategory = recipeCategoryRepository.save(recipeCategory);
        return recipeCategoryMapper.mapToRecipeCategoryDto(recipeCategory);
    }

    @Override
    public RecipeCategoryDto update(UpdateRecipeCategoryDto updateRecipeCategoryDto) {
        RecipeCategory recipeCategory =
                recipeCategoryRepository.findById(updateRecipeCategoryDto.getId())
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeCategoryErrorCode.recipeCategoryNotFoundErrorCode, RecipeCategoryErrorMessage.recipeCategoryNotFoundErrorMessage));

        if(!updateRecipeCategoryDto.getName().equals(recipeCategory.getName())) {
            if(recipeCategoryRepository.existsByName(updateRecipeCategoryDto.getName())) {
                throw new DuplicatedResourceException(RecipeCategoryErrorCode.duplicatedRecipeCategoryNameErrorCode,
                        RecipeCategoryErrorMessage.duplicatedRecipeCategoryErrorMessage);
            }
            recipeCategory.setName(updateRecipeCategoryDto.getName());
        }
        if(!updateRecipeCategoryDto.getPolishName().equals(recipeCategory.getPolishName())) {
            if(recipeCategoryRepository.existsByPolishName(updateRecipeCategoryDto.getPolishName())) {
                throw new DuplicatedResourceException(RecipeCategoryErrorCode.duplicatedRecipeCategoryPolishNameErrorCode,
                        RecipeCategoryErrorMessage.duplicatedRecipeCategoryErrorMessage);
            }
            recipeCategory.setPolishName(updateRecipeCategoryDto.getPolishName());
        }

        recipeCategory.setParentId(updateRecipeCategoryDto.getParentId());
        recipeCategoryRepository.save(recipeCategory);
        return recipeCategoryMapper.mapToRecipeCategoryDto(recipeCategory);
    }

    @Override
    public void delete(UUID id) {
        RecipeCategory recipeCategory =
                recipeCategoryRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeCategoryErrorCode.recipeCategoryNotFoundErrorCode, RecipeCategoryErrorMessage.recipeCategoryNotFoundErrorMessage));

        recipeCategoryRepository.delete(recipeCategory);
    }
}
