package com.culinaryAntidote.recipeCategories.web;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategorySearchCriteria;
import com.culinaryAntidote.recipeCategories.services.CreateRecipeCategoryDto;
import com.culinaryAntidote.recipeCategories.services.RecipeCategoryDto;
import com.culinaryAntidote.recipeCategories.services.RecipeCategoryService;
import com.culinaryAntidote.recipeCategories.services.UpdateRecipeCategoryDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("recipeCategories")
@AllArgsConstructor
public class RecipeCategoryController {

    private final RecipeCategoryService recipeCategoryService;

    @GetMapping
    public Page<RecipeCategoryDto> getRecipeCategories(Pageable pageable, RecipeCategorySearchCriteria criteria) {
        return recipeCategoryService.findAll(pageable, criteria);
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('Admin')")
    public RecipeCategoryDto getRecipeCategory(@PathVariable("id") UUID id) {
        return recipeCategoryService.findById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<RecipeCategoryDto> createRecipeCategory(@Valid @RequestBody CreateRecipeCategoryDto createRecipeCategory, UriComponentsBuilder uriComponentsBuilder) {
        RecipeCategoryDto recipeCategoryDto =  recipeCategoryService.create(createRecipeCategory);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/recipeCategories/{id}").buildAndExpand(recipeCategoryDto.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(recipeCategoryDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('Admin')")
    public RecipeCategoryDto updateRecipeCategory(@PathVariable("id") UUID id,
                                                  @Valid @RequestBody UpdateRecipeCategoryDto updateRecipeCategoryDto) {

        if(!id.equals(updateRecipeCategoryDto.getId())) {
            throw new ValidationException(GeneralErrorCode.idMismatchErrorCode,
                    GeneralErrorMessage.idMismatchErrorMessage);
        }
        return recipeCategoryService.update(updateRecipeCategoryDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity deleteRecipeCategory(@PathVariable("id") UUID id) {
        recipeCategoryService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
