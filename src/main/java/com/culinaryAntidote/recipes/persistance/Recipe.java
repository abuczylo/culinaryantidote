package com.culinaryAntidote.recipes.persistance;

import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import com.culinaryAntidote.users.persistance.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(schema="public", name="recipes")
public class Recipe {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "portions_amount", nullable = false)
    private int portionsAmount;

    @Column(name = "difficulty_level", nullable = false)
    @Enumerated(EnumType.STRING)
    private DifficultyLevel difficultyLevel;

    @Column(name = "preparation_time", nullable = false)
    private int preparationTime;

    @Column(name = "ingredients", nullable = false)
    private String ingredients;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "creation_date", nullable = false)
    private OffsetDateTime creationDate;

    @Column(name = "update_date")
    private OffsetDateTime updateDate;

    @Column(name = "average_rating", precision = 3, scale = 2)
    private Double averageRating;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;

    @ManyToMany
    @JoinTable(
            name = "recipe_recipe_categories",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "recipe_category_id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<RecipeCategory> recipeCategories = new HashSet<>();

    @ManyToMany(mappedBy = "favouriteRecipes")
    private Set<User> userFavourites = new HashSet<>();
}
