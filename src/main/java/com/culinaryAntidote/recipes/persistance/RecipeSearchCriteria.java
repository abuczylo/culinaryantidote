package com.culinaryAntidote.recipes.persistance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecipeSearchCriteria {

    private String phrase;
    private Optional<Integer> portionsAmount;
    private Optional<DifficultyLevel> difficultyLevel;
    private Optional<Integer> preparationTimeFrom;
    private Optional<Integer> preparationTimeTo;
    private String includedIngredients;
    private String excludedIngredients;
    private UUID recipeCategoryId;
}
