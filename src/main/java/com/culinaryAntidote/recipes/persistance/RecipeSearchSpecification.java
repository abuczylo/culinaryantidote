package com.culinaryAntidote.recipes.persistance;


import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@AllArgsConstructor
public class RecipeSearchSpecification implements Specification<Recipe> {

    private static final String SEARCH_PATTERN = "%%%s%%";
    private final RecipeSearchCriteria searchCriteria;
    private final List<Predicate> predicates = new LinkedList<>();

    @Override
    public Predicate toPredicate(Root<Recipe> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        filterByPhrase(root, criteriaBuilder);
        filterByPortionsAmount(root, criteriaBuilder);
        filterByDifficultyLevel(root, criteriaBuilder);
        filterByPreparationTime(root, criteriaBuilder);
        filterByIncludedIngredients(root, criteriaBuilder);
        filterByExcludedIngredients(root, criteriaBuilder);
        filterByRecipeCategory(root, criteriaQuery, criteriaBuilder);

        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
    }

    private void filterByPhrase(Root<Recipe> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getPhrase() != null) {
            Predicate namePredicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("name")),
                    format(SEARCH_PATTERN, searchCriteria.getPhrase().toLowerCase())
            );
            Predicate descriptionPredicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("description")),
                    format(SEARCH_PATTERN, searchCriteria.getPhrase().toLowerCase())
            );
            Predicate predicate = criteriaBuilder.or(namePredicate, descriptionPredicate);
            predicates.add(predicate);
        }
    }

    private void filterByPortionsAmount(Root<Recipe> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getPortionsAmount() != null) {
            Predicate predicate = criteriaBuilder.equal(
                    root.get("portionsAmount"), searchCriteria.getPortionsAmount().get());
            predicates.add(predicate);
        }
    }

    private void filterByDifficultyLevel(Root<Recipe>root, CriteriaBuilder criteriaBuilder){
        if (searchCriteria.getDifficultyLevel() != null) {
            Predicate predicate = criteriaBuilder.equal(
                    root.get("difficultyLevel"), searchCriteria.getDifficultyLevel().get());
            predicates.add(predicate);
        }
    }

    private void filterByPreparationTime(Root<Recipe>root, CriteriaBuilder criteriaBuilder){
        if (searchCriteria.getPreparationTimeFrom() != null && searchCriteria.getPreparationTimeTo() != null) {
            Predicate greaterThanOrEqualToPredicate = criteriaBuilder.greaterThanOrEqualTo(
                    root.get("preparationTime"), searchCriteria.getPreparationTimeFrom().get());
            Predicate lessThanOrEqualToPredicate = criteriaBuilder.lessThanOrEqualTo(
                    root.get("preparationTime"), searchCriteria.getPreparationTimeTo().get());
            predicates.add(greaterThanOrEqualToPredicate);
            predicates.add(lessThanOrEqualToPredicate);
        }
    }

    private void filterByIncludedIngredients(Root<Recipe> root, CriteriaBuilder criteriaBuilder) {
        if(searchCriteria.getIncludedIngredients() != null) {
            String[] includedIngredients = searchCriteria.getIncludedIngredients().trim().split(",");

            for (String includedIngredient: includedIngredients) {
                Predicate predicate = criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("ingredients")),
                        format(SEARCH_PATTERN, includedIngredient.toLowerCase()));
                predicates.add(predicate);
            }
        }
    }

    private void filterByExcludedIngredients(Root<Recipe> root, CriteriaBuilder criteriaBuilder) {
        if(searchCriteria.getExcludedIngredients() != null) {
            String[] excludedIngredients = searchCriteria.getExcludedIngredients().trim().split(",");

            for (String excludedIngredient: excludedIngredients) {
                Predicate predicate = criteriaBuilder.notLike(
                        criteriaBuilder.lower(root.get("ingredients")),
                        format(SEARCH_PATTERN, excludedIngredient.toLowerCase()));
                predicates.add(predicate);
            }
        }
    }

    private void filterByRecipeCategory(Root<Recipe> root, CriteriaQuery<?> criteriaQuery,
                                    CriteriaBuilder criteriaBuilder) {
        if(searchCriteria.getRecipeCategoryId() != null) {
            criteriaQuery.distinct(true);
            Subquery<RecipeCategory> recipeCategorySubQuery = criteriaQuery.subquery(RecipeCategory.class);
            Root<RecipeCategory> recipeCategoryRoot = recipeCategorySubQuery.from(RecipeCategory.class);
            Expression<Collection<Recipe>> recipeRecipeCategories = recipeCategoryRoot.get("recipes");
            recipeCategorySubQuery.select(recipeCategoryRoot);
            recipeCategorySubQuery.where(criteriaBuilder.equal(recipeCategoryRoot.get("id"),
                    searchCriteria.getRecipeCategoryId()),
                    criteriaBuilder.isMember(root, recipeRecipeCategories));
            Predicate predicate =  criteriaBuilder.exists(recipeCategorySubQuery);
            predicates.add(predicate);
        }
    }
}
