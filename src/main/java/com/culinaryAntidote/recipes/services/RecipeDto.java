package com.culinaryAntidote.recipes.services;

import com.culinaryAntidote.recipes.persistance.DifficultyLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RecipeDto {

    private UUID id;
    private String name;
    private int portionsAmount;
    private DifficultyLevel difficultyLevel;
    private int preparationTime;
    private String ingredients;
    private String description;
    private OffsetDateTime creationDate;
    private OffsetDateTime updateDate;
    private Double averageRating;
    private UUID userId;
    private List<UUID> recipeCategories;
}
