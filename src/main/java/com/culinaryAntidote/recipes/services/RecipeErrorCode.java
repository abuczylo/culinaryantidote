package com.culinaryAntidote.recipes.services;

public class RecipeErrorCode {

    public static final String recipeNotFoundErrorCode = "RecipeNotFound";
    public static final String recipeCategoryNotFoundErrorCode = "RecipeCategoryNotFoundErrorCode";
}
