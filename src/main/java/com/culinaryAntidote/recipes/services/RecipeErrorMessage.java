package com.culinaryAntidote.recipes.services;

public class RecipeErrorMessage {

    public static final String recipeNotFoundErrorMessage = "Recipe not found.";
    public static final String recipeCategoryNotFoundErrorMessage = "Recipe category not found.";
}
