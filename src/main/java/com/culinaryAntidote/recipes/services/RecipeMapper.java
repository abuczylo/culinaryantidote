package com.culinaryAntidote.recipes.services;

import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import com.culinaryAntidote.recipes.persistance.Recipe;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface RecipeMapper {

    @Mapping(source = "user.id", target = "userId")
    RecipeDto mapToRecipeDto(Recipe recipe);

    @Mapping(source = "recipeCategories", target = "recipeCategories", ignore = true)
    @Mapping(source = "userId", target = "user", ignore = true)
    Recipe mapToRecipe(CreateRecipeDto createRecipeDto);

    default List<UUID> mapToRecipeCategories(Set<RecipeCategory> recipeCategories) {
        if (recipeCategories == null) {
            return new ArrayList<>();
        }
        return recipeCategories.stream().map(recipeCategory -> recipeCategory.getId()).collect(Collectors.toList());
    }
}
