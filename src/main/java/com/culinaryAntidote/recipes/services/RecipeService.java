package com.culinaryAntidote.recipes.services;

import com.culinaryAntidote.recipes.persistance.RecipeSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface RecipeService {

    Page<RecipeDto> findAll(Pageable pageable, RecipeSearchCriteria criteria);
    RecipeDto findById(UUID id);
    RecipeDto create(CreateRecipeDto createRecipeDto);
    RecipeDto update(UpdateRecipeDto updateRecipeDto);
    void delete(UUID id);
}
