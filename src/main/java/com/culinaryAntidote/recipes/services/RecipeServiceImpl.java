package com.culinaryAntidote.recipes.services;

import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategoryRepository;
import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.recipes.persistance.RecipeSearchCriteria;
import com.culinaryAntidote.recipes.persistance.RecipeSearchSpecification;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import com.culinaryAntidote.users.services.UserErrorCode;
import com.culinaryAntidote.users.services.UserErrorMessage;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.time.OffsetDateTime.now;

@Service
@AllArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private RecipeRepository recipeRepository;
    private RecipeCategoryRepository recipeCategoryRepository;
    private UserRepository userRepository;
    private RecipeMapper recipeMapper;

    @Override
    public Page<RecipeDto> findAll(Pageable pageable, RecipeSearchCriteria criteria) {
        RecipeSearchSpecification specification = new RecipeSearchSpecification(criteria);
        Page<Recipe> pagedRecipes = recipeRepository.findAll(specification, pageable);
        return pagedRecipes.map(recipe -> recipeMapper.mapToRecipeDto(recipe));
    }

    @Override
    public RecipeDto findById(UUID id) {
        Recipe recipe =
                recipeRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeErrorCode.recipeNotFoundErrorCode, RecipeErrorMessage.recipeNotFoundErrorMessage));
        return recipeMapper.mapToRecipeDto(recipe);
    }

    @Override
    public RecipeDto create(CreateRecipeDto createRecipeDto) {
        Set<RecipeCategory> recipeCategories = getRecipeCategoriesForRecipe(createRecipeDto.getRecipeCategories());
        User user = getUser(createRecipeDto.getUserId());
        Recipe recipe = recipeMapper.mapToRecipe(createRecipeDto);
        recipe.setUser(user);
        recipe.setCreationDate(now());
        recipe.setRecipeCategories(recipeCategories);
        recipe = recipeRepository.save(recipe);
        return recipeMapper.mapToRecipeDto(recipe);
    }

    @Override
    public RecipeDto update(UpdateRecipeDto updateRecipeDto) {
        Recipe recipe =
                recipeRepository.findById(updateRecipeDto.getId())
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeErrorCode.recipeNotFoundErrorCode, RecipeErrorMessage.recipeNotFoundErrorMessage));

        Set<RecipeCategory> recipeCategories = getRecipeCategoriesForRecipe(updateRecipeDto.getRecipeCategories());
        User user = getUser(updateRecipeDto.getUserId());
        recipe.setUser(user);
        recipe.setUpdateDate(now());
        recipe.setRecipeCategories(recipeCategories);
        recipe.setDescription(updateRecipeDto.getDescription());
        recipe.setDifficultyLevel(updateRecipeDto.getDifficultyLevel());
        recipe.setIngredients(updateRecipeDto.getIngredients());
        recipe.setName(updateRecipeDto.getName());
        recipe.setPortionsAmount(updateRecipeDto.getPortionsAmount());
        recipe.setPreparationTime(updateRecipeDto.getPreparationTime());
        recipe = recipeRepository.save(recipe);
        return recipeMapper.mapToRecipeDto(recipe);
    }

    @Override
    public void delete(UUID id) {
        Recipe recipe =
                recipeRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RecipeErrorCode.recipeNotFoundErrorCode, RecipeErrorMessage.recipeNotFoundErrorMessage));

        recipeRepository.delete(recipe);
    }

    private Set<RecipeCategory> getRecipeCategoriesForRecipe(List<UUID> recipeCategoryIds) {
        Set<RecipeCategory> recipeCategories = new HashSet<>();

        for (UUID recipeCategoryId : recipeCategoryIds) {
            RecipeCategory recipeCategory = recipeCategoryRepository.findById(recipeCategoryId)
                    .orElseThrow(() -> new ValidationException(RecipeErrorCode.recipeCategoryNotFoundErrorCode,
                            RecipeErrorMessage.recipeCategoryNotFoundErrorMessage));
            recipeCategories.add(recipeCategory);
        }

        return recipeCategories;
    }

    private User getUser(UUID userId) {
        return
                userRepository.findById(userId)
                        .orElseThrow(() -> new ValidationException(UserErrorCode.userNotFoundErrorCode,
                                UserErrorMessage.userNotFoundErrorMessage));
    }
}
