package com.culinaryAntidote.recipes.services;

import com.culinaryAntidote.recipes.persistance.DifficultyLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateRecipeDto {

    @NotNull(message = "Id is required.")
    private UUID id;

    @NotBlank(message = "Name is required.")
    private String name;

    @NotNull(message = "PortionsAmount is required.")
    @Min(1)
    private int portionsAmount;

    @NotNull(message = "DifficultyLevel is required.")
    private DifficultyLevel difficultyLevel;

    @NotNull(message = "PortionsAmount is required.")
    @Min(1)
    private int preparationTime;

    @NotBlank(message = "Ingredients is required.")
    private String ingredients;

    @NotBlank(message = "Description is required.")
    private String description;

    @NotNull(message = "UserId is required.")
    private UUID userId;

    @NotNull(message = "RecipeCategories are required.")
    @NotEmpty(message = "RecipeCategories are required.")
    private List<UUID> recipeCategories;
}
