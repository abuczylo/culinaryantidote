package com.culinaryAntidote.recipes.web;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.recipes.persistance.RecipeSearchCriteria;
import com.culinaryAntidote.recipes.services.CreateRecipeDto;
import com.culinaryAntidote.recipes.services.RecipeDto;
import com.culinaryAntidote.recipes.services.RecipeService;
import com.culinaryAntidote.recipes.services.UpdateRecipeDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("recipes")
@AllArgsConstructor
public class RecipeController {

    private RecipeService recipeService;

    @GetMapping
    public Page<RecipeDto> getRecipes(Pageable pageable, RecipeSearchCriteria criteria) {
        return recipeService.findAll(pageable, criteria);
    }

    @GetMapping("{id}")
    public RecipeDto getRecipe(@PathVariable("id") UUID id) {
        return recipeService.findById(id);
    }

    @PostMapping
    public ResponseEntity<RecipeDto> createRecipe(@Valid @RequestBody CreateRecipeDto createRecipeDto,
                                                  UriComponentsBuilder uriComponentsBuilder) {
        RecipeDto recipeDto =  recipeService.create(createRecipeDto);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/recipes/{id}").buildAndExpand(recipeDto.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(recipeDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userResourceOwnerCheck.check(#updateRecipeDto.getUserId(), authentication" +
            ".getName())")
    public RecipeDto updateRecipe(@PathVariable("id") UUID id,
                                  @Valid @RequestBody UpdateRecipeDto updateRecipeDto) {

        if(!id.equals(updateRecipeDto.getId())) {
            throw new ValidationException(GeneralErrorCode.idMismatchErrorCode,
                    GeneralErrorMessage.idMismatchErrorMessage);
        }
        return recipeService.update(updateRecipeDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @recipeResourceOwnerCheck.check(#id, authentication.getName())")
    public ResponseEntity deleteRecipe(@PathVariable("id") UUID id) {
        recipeService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
