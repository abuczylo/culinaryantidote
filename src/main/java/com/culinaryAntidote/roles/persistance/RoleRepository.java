package com.culinaryAntidote.roles.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, UUID>,
        JpaRepository<Role, UUID>, JpaSpecificationExecutor<Role> {

    boolean existsByName(String name);
    Role findByName(String name);
}
