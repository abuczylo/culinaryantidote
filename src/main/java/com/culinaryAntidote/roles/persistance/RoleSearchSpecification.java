package com.culinaryAntidote.roles.persistance;


import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;

@AllArgsConstructor
public class RoleSearchSpecification implements Specification<Role> {

    private static final String SEARCH_PATTERN = "%%%s%%";
    private final RoleSearchCriteria searchCriteria;
    private final List<Predicate> predicates = new LinkedList<>();

    @Override
    public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        filterByName(root, criteriaBuilder);
        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
    }

    private void filterByName(Root<Role> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getName() != null) {
            Predicate namePredicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("name")),
                    format(SEARCH_PATTERN, searchCriteria.getName().toLowerCase())
            );
            predicates.add(namePredicate);
        }
    }
}
