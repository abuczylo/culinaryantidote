package com.culinaryAntidote.roles.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateRoleDto {

    @NotBlank(message = "Name is required.")
    private String name;
}
