package com.culinaryAntidote.roles.services;

public class RoleErrorCode {

    public static final String roleNotFoundErrorCode = "RoleNotFound";
    public static final String duplicatedRoleErrorCode = "DuplicatedRole";
}
