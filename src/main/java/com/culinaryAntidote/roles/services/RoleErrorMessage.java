package com.culinaryAntidote.roles.services;

public class RoleErrorMessage {

    public static final String roleNotFoundErrorMessage = "Role not found.";
    public static final String duplicatedRoleErrorMessage = "Role already exist.";
}
