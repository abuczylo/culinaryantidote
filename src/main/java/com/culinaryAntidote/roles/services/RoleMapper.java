package com.culinaryAntidote.roles.services;

import com.culinaryAntidote.roles.persistance.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleDto mapToRoleDto(Role role);
    Role mapToRole(CreateRoleDto createRole);
}
