package com.culinaryAntidote.roles.services;

import com.culinaryAntidote.roles.persistance.RoleSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface RoleService {

    Page<RoleDto> findAll(Pageable pageable, RoleSearchCriteria criteria);
    RoleDto findById(UUID id);
    RoleDto create(CreateRoleDto createRoleDto);
    RoleDto update(UpdateRoleDto updateRoleDto);
    void delete(UUID id);
}
