package com.culinaryAntidote.roles.services;

import com.culinaryAntidote.common.exceptions.DuplicatedResourceException;
import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.roles.persistance.Role;
import com.culinaryAntidote.roles.persistance.RoleRepository;
import com.culinaryAntidote.roles.persistance.RoleSearchCriteria;
import com.culinaryAntidote.roles.persistance.RoleSearchSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;
    private RoleMapper roleMapper;
    
    @Override
    public Page<RoleDto> findAll(Pageable pageable, RoleSearchCriteria criteria) {
        RoleSearchSpecification specification = new RoleSearchSpecification(criteria);
        Page<Role> pagedRecipeCategories = roleRepository.findAll(specification, pageable);
        return pagedRecipeCategories.map(Role -> roleMapper.mapToRoleDto(Role));
    }

    @Override
    public RoleDto findById(UUID id) {
        Role role =
                roleRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RoleErrorCode.roleNotFoundErrorCode,
                                RoleErrorMessage.roleNotFoundErrorMessage));
        return roleMapper.mapToRoleDto(role);
    }

    @Override
    public RoleDto create(CreateRoleDto createRoleDto) {
        if(roleRepository.existsByName(createRoleDto.getName())) {
            throw new DuplicatedResourceException(RoleErrorCode.duplicatedRoleErrorCode,
                    RoleErrorMessage.duplicatedRoleErrorMessage);
        }

        Role role = roleMapper.mapToRole(createRoleDto);
        role = roleRepository.save(role);
        return roleMapper.mapToRoleDto(role);
    }

    @Override
    public RoleDto update(UpdateRoleDto updateRoleDto) {
        Role role =
                roleRepository.findById(updateRoleDto.getId())
                        .orElseThrow(() -> new ResourceNotFoundException(RoleErrorCode.roleNotFoundErrorCode,
                                RoleErrorMessage.roleNotFoundErrorMessage));

        if(!updateRoleDto.getName().equals(role.getName())) {
            if(roleRepository.existsByName(updateRoleDto.getName())) {
                throw new DuplicatedResourceException(RoleErrorCode.duplicatedRoleErrorCode,
                        RoleErrorMessage.duplicatedRoleErrorMessage);
            }
            role.setName(updateRoleDto.getName());
            role = roleRepository.save(role);
        }

        return roleMapper.mapToRoleDto(role);
    }

    @Override
    public void delete(UUID id) {
        Role role =
                roleRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(RoleErrorCode.roleNotFoundErrorCode,
                                RoleErrorMessage.roleNotFoundErrorMessage));
        roleRepository.delete(role);
    }
}
