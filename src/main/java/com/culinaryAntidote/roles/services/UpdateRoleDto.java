package com.culinaryAntidote.roles.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateRoleDto {

    @NotNull(message = "Id is required.")
    private UUID id;

    @NotBlank(message = "Name is required.")
    private String name;
}
