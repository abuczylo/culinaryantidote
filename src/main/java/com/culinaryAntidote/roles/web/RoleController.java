package com.culinaryAntidote.roles.web;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.roles.persistance.RoleSearchCriteria;
import com.culinaryAntidote.roles.services.CreateRoleDto;
import com.culinaryAntidote.roles.services.RoleDto;
import com.culinaryAntidote.roles.services.RoleService;
import com.culinaryAntidote.roles.services.UpdateRoleDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("roles")
@AllArgsConstructor
@PreAuthorize("hasRole('Admin')")
public class RoleController {

    private final RoleService roleService;

    @GetMapping
    public Page<RoleDto> getRecipeCategories(Pageable pageable, RoleSearchCriteria criteria) {
        return roleService.findAll(pageable, criteria);
    }

    @GetMapping("{id}")
    public RoleDto getRole(@PathVariable("id") UUID id) {
        return roleService.findById(id);
    }

    @PostMapping
    public ResponseEntity<RoleDto> createRole(@Valid @RequestBody CreateRoleDto createRoleDto,
                                              UriComponentsBuilder uriComponentsBuilder) {
        RoleDto RoleDto =  roleService.create(createRoleDto);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/roles/{id}").buildAndExpand(RoleDto.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(RoleDto);
    }

    @PutMapping("{id}")
    public RoleDto updateRole(@PathVariable("id") UUID id,
                              @Valid @RequestBody UpdateRoleDto updateRoleDto) {

        if(!id.equals(updateRoleDto.getId())) {
            throw new ValidationException(GeneralErrorCode.idMismatchErrorCode,
                    GeneralErrorMessage.idMismatchErrorMessage);
        }
        return roleService.update(updateRoleDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteRole(@PathVariable("id") UUID id) {
        roleService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
