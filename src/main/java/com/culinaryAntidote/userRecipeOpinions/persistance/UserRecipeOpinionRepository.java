package com.culinaryAntidote.userRecipeOpinions.persistance;

import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.users.persistance.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRecipeOpinionRepository extends PagingAndSortingRepository<UserRecipeOpinion, UUID>,
        JpaRepository<UserRecipeOpinion, UUID>, JpaSpecificationExecutor<UserRecipeOpinion> {

    List<UserRecipeOpinion> findAllByRecipe(Recipe recipe);
    boolean existsByUser(User user);
    boolean existsByRecipe(Recipe recipe);
}
