package com.culinaryAntidote.userRecipeOpinions.persistance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRecipeOpinionSearchCriteria {

    private UUID recipeId;
}
