package com.culinaryAntidote.userRecipeOpinions.persistance;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
public class UserRecipeOpinionSearchSpecification implements Specification<UserRecipeOpinion> {

    private final UserRecipeOpinionSearchCriteria searchCriteria;
    private final List<Predicate> predicates = new LinkedList<>();

    @Override
    public Predicate toPredicate(Root<UserRecipeOpinion> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        filterByRecipeId(root, criteriaBuilder);
        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
    }

    private void filterByRecipeId(Root<UserRecipeOpinion> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getRecipeId() != null) {
            Predicate namePredicate = criteriaBuilder.equal(root.get("recipeId"), searchCriteria.getRecipeId());
            predicates.add(namePredicate);
        }
    }
}
