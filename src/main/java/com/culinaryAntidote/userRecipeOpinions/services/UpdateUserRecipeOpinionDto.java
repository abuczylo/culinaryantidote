package com.culinaryAntidote.userRecipeOpinions.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateUserRecipeOpinionDto {

    @NotNull(message = "Id is required.")
    private UUID id;

    @NotNull(message = "Rating is required.")
    @Min(value = 1, message = "Rating min value is 1.")
    @Max(value = 5, message = "Rating max value is 5.")
    private int rating;

    @NotBlank(message = "Name is required.")
    private String comment;

    @NotNull(message = "UserId is required.")
    private UUID userId;

    @NotNull(message = "RecipeId is required.")
    private UUID recipeId;
}
