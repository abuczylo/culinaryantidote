package com.culinaryAntidote.userRecipeOpinions.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserRecipeOpinionDto {

    private UUID id;
    private int rating;
    private String comment;
    private UUID userId;
    private UUID recipeId;
}
