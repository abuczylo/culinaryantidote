package com.culinaryAntidote.userRecipeOpinions.services;

public class UserRecipeOpinionErrorCode {

    public static final String userRecipeOpinionNotFoundErrorCode = "UserRecipeOpinionNotFound";
    public static final String duplicatedUserRecipeOpinionErrorCode = "DuplicatedUserRecipeOpinion";
    public static final String recipeNotFoundErrorCode = "RecipeNotFound";
    public static final String userNotFoundErrorCode = "UserNotFound";
}
