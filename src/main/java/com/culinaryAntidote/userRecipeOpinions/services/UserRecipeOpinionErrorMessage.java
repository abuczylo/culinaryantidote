package com.culinaryAntidote.userRecipeOpinions.services;

public class UserRecipeOpinionErrorMessage {

    public static final String userRecipeOpinionNotFoundErrorMessage  = "User recipe opinion not found.";
    public static final String duplicatedUserRecipeOpinionErrorMessage  = "User recipe opinion already exist.";
    public static final String recipeNotFoundErrorMessage = "Recipe not found";
    public static final String userNotFoundErrorMessage = "User not found";
}
