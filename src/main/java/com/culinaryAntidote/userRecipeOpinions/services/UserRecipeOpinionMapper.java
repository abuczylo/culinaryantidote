package com.culinaryAntidote.userRecipeOpinions.services;

import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserRecipeOpinionMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "recipe.id", target = "recipeId")
    UserRecipeOpinionDto mapToUserRecipeOpinionDto(UserRecipeOpinion userRecipeOpinion);
}
