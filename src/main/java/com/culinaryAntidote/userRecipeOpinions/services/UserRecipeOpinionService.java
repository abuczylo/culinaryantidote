package com.culinaryAntidote.userRecipeOpinions.services;

import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface UserRecipeOpinionService {

    Page<UserRecipeOpinionDto> findAll(Pageable pageable, UserRecipeOpinionSearchCriteria criteria);
    UserRecipeOpinionDto findById(UUID id);
    UserRecipeOpinionDto create(CreateUserRecipeOpinionDto createUserRecipeOpinionDto);
    UserRecipeOpinionDto update(UpdateUserRecipeOpinionDto updateUserRecipeOpinionDto);
    void delete(UUID id);
}
