package com.culinaryAntidote.userRecipeOpinions.services;

import com.culinaryAntidote.common.exceptions.DuplicatedResourceException;
import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinion;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionRepository;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionSearchCriteria;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionSearchSpecification;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserRecipeOpinionServiceImpl implements UserRecipeOpinionService {

    private UserRecipeOpinionRepository userRecipeOpinionRepository;
    private UserRepository userRepository;
    private RecipeRepository recipeRepository;
    private UserRecipeOpinionMapper userRecipeOpinionMapper;

    @Override
    public Page<UserRecipeOpinionDto> findAll(Pageable pageable, UserRecipeOpinionSearchCriteria criteria) {
        UserRecipeOpinionSearchSpecification specification = new UserRecipeOpinionSearchSpecification(criteria);
        Page<UserRecipeOpinion> pagedUserRecipeOpinions = userRecipeOpinionRepository.findAll(specification, pageable);
        return pagedUserRecipeOpinions.map(userRecipeOpinion -> userRecipeOpinionMapper.mapToUserRecipeOpinionDto(userRecipeOpinion));
    }

    @Override
    public UserRecipeOpinionDto findById(UUID id) {
        UserRecipeOpinion userRecipeOpinion =
                userRecipeOpinionRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userRecipeOpinionNotFoundErrorCode,
                                UserRecipeOpinionErrorMessage.userRecipeOpinionNotFoundErrorMessage));
        return userRecipeOpinionMapper.mapToUserRecipeOpinionDto(userRecipeOpinion);
    }

    @Override
    public UserRecipeOpinionDto create(CreateUserRecipeOpinionDto createUserRecipeOpinionDto) {
        User user =
                userRepository.findById(createUserRecipeOpinionDto.getUserId()).orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userNotFoundErrorCode, UserRecipeOpinionErrorMessage.userNotFoundErrorMessage));
        Recipe recipe =
                recipeRepository.findById(createUserRecipeOpinionDto.getRecipeId()).orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.recipeNotFoundErrorCode, UserRecipeOpinionErrorMessage.recipeNotFoundErrorMessage));

        if(userRecipeOpinionRepository.existsByUser(user) && userRecipeOpinionRepository.existsByRecipe(recipe)) {
            throw new DuplicatedResourceException(UserRecipeOpinionErrorCode.duplicatedUserRecipeOpinionErrorCode,
                    UserRecipeOpinionErrorMessage.duplicatedUserRecipeOpinionErrorMessage);
        }

        UserRecipeOpinion userRecipeOpinion = UserRecipeOpinion.builder()
                .rating(createUserRecipeOpinionDto.getRating())
                .comment(createUserRecipeOpinionDto.getComment())
                .user(user)
                .recipe(recipe)
                .build();
        userRecipeOpinion = userRecipeOpinionRepository.save(userRecipeOpinion);
        updateRecipeAverageRating(recipe);

        return userRecipeOpinionMapper.mapToUserRecipeOpinionDto(userRecipeOpinion);
    }

    @Override
    public UserRecipeOpinionDto update(UpdateUserRecipeOpinionDto updateUserRecipeOpinionDto) {
        UserRecipeOpinion userRecipeOpinion =
                userRecipeOpinionRepository.findById(updateUserRecipeOpinionDto.getId())
                        .orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userRecipeOpinionNotFoundErrorCode,
                                UserRecipeOpinionErrorMessage.userRecipeOpinionNotFoundErrorMessage));
        boolean isDifferentRecipe =
                !userRecipeOpinion.getRecipe().getId().equals(updateUserRecipeOpinionDto.getRecipeId());
        boolean isDifferentUser = !userRecipeOpinion.getUser().getId().equals(updateUserRecipeOpinionDto.getUserId());

        if(isDifferentRecipe || isDifferentUser) {
            User user =
                    userRepository.findById(updateUserRecipeOpinionDto.getUserId()).orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userNotFoundErrorCode, UserRecipeOpinionErrorMessage.userNotFoundErrorMessage));
            Recipe recipe =
                    recipeRepository.findById(updateUserRecipeOpinionDto.getRecipeId()).orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.recipeNotFoundErrorCode, UserRecipeOpinionErrorMessage.recipeNotFoundErrorMessage));

            if(userRecipeOpinionRepository.existsByUser(user) && userRecipeOpinionRepository.existsByRecipe(recipe)) {
                throw new DuplicatedResourceException(UserRecipeOpinionErrorCode.duplicatedUserRecipeOpinionErrorCode,
                        UserRecipeOpinionErrorMessage.duplicatedUserRecipeOpinionErrorMessage);
            }

            userRecipeOpinion.setUser(user);
            userRecipeOpinion.setRecipe(recipe);
        }

        userRecipeOpinion.setRating(updateUserRecipeOpinionDto.getRating());
        userRecipeOpinion.setComment(updateUserRecipeOpinionDto.getComment());
        userRecipeOpinionRepository.save(userRecipeOpinion);
        updateRecipeAverageRating(userRecipeOpinion.getRecipe());

        return userRecipeOpinionMapper.mapToUserRecipeOpinionDto(userRecipeOpinion);
    }

    @Override
    public void delete(UUID id) {
        UserRecipeOpinion userRecipeOpinion =
                userRecipeOpinionRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(UserRecipeOpinionErrorCode.userRecipeOpinionNotFoundErrorCode,
                                UserRecipeOpinionErrorMessage.userRecipeOpinionNotFoundErrorMessage));
        userRecipeOpinionRepository.delete(userRecipeOpinion);
        updateRecipeAverageRating(userRecipeOpinion.getRecipe());
    }

    private void updateRecipeAverageRating(Recipe recipe) {
        List<UserRecipeOpinion> recipeOpinions = userRecipeOpinionRepository.findAllByRecipe(recipe);
        double averageRating =
                recipeOpinions.stream().mapToDouble(UserRecipeOpinion::getRating).sum() / recipeOpinions.size();
        recipe.setAverageRating(averageRating);
        recipeRepository.save(recipe);
    }
}
