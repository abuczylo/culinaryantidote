package com.culinaryAntidote.userRecipeOpinions.web;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.userRecipeOpinions.persistance.UserRecipeOpinionSearchCriteria;
import com.culinaryAntidote.userRecipeOpinions.services.CreateUserRecipeOpinionDto;
import com.culinaryAntidote.userRecipeOpinions.services.UpdateUserRecipeOpinionDto;
import com.culinaryAntidote.userRecipeOpinions.services.UserRecipeOpinionDto;
import com.culinaryAntidote.userRecipeOpinions.services.UserRecipeOpinionService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("userRecipeOpinions")
@AllArgsConstructor
public class UserRecipeOpinionController {

    private final UserRecipeOpinionService userRecipeOpinionService;

    @GetMapping
    public Page<UserRecipeOpinionDto> getUserRecipeOpinions(Pageable pageable, UserRecipeOpinionSearchCriteria criteria) {
        return userRecipeOpinionService.findAll(pageable, criteria);
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userRecipeOpinionResourceOwnerCheck.check(#id, authentication.getName())")
    public UserRecipeOpinionDto getUserRecipeOpinion(@PathVariable("id") UUID id) {
        return userRecipeOpinionService.findById(id);
    }

    @PostMapping
    public ResponseEntity<UserRecipeOpinionDto> createUserRecipeOpinion(@Valid @RequestBody CreateUserRecipeOpinionDto createUserRecipeOpinionDto,
                                              UriComponentsBuilder uriComponentsBuilder) {
        UserRecipeOpinionDto userRecipeOpinionDto =  userRecipeOpinionService.create(createUserRecipeOpinionDto);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/userRecipeOpinions/{id}").buildAndExpand(userRecipeOpinionDto.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(userRecipeOpinionDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userResourceOwnerCheck.check(#updateUserRecipeOpinionDto.getUserId(), authentication" +
            ".getName())")
    public UserRecipeOpinionDto updateUserRecipeOpinion(@PathVariable("id") UUID id,
                                                        @Valid @RequestBody UpdateUserRecipeOpinionDto updateUserRecipeOpinionDto) {

        if(!id.equals(updateUserRecipeOpinionDto.getId())) {
            throw new ValidationException(GeneralErrorCode.idMismatchErrorCode,
                    GeneralErrorMessage.idMismatchErrorMessage);
        }
        return userRecipeOpinionService.update(updateUserRecipeOpinionDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userRecipeOpinionResourceOwnerCheck.check(#id, authentication.getName())")
    public ResponseEntity deleteUserRecipeOpinion(@PathVariable("id") UUID id) {
        userRecipeOpinionService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
