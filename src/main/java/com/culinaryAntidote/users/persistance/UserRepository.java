package com.culinaryAntidote.users.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, UUID>,
        JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {

    List<User> findAllByEmail(String email);
    boolean existsByEmail(String email);
}
