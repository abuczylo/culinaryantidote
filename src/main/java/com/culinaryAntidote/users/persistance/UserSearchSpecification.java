package com.culinaryAntidote.users.persistance;


import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;

@AllArgsConstructor
public class UserSearchSpecification implements Specification<User> {

    private static final String SEARCH_PATTERN = "%%%s%%";
    private final UserSearchCriteria searchCriteria;
    private final List<Predicate> predicates = new LinkedList<>();

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        filterByEmail(root, criteriaBuilder);
        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
    }

    private void filterByEmail(Root<User> root, CriteriaBuilder criteriaBuilder) {
        if (searchCriteria.getEmail() != null) {
            Predicate namePredicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root.get("email")),
                    format(SEARCH_PATTERN, searchCriteria.getEmail().toLowerCase())
            );
            predicates.add(namePredicate);
        }
    }
}
