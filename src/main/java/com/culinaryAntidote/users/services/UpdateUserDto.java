package com.culinaryAntidote.users.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateUserDto {

    @NotNull(message = "Id is required.")
    private UUID id;

    @NotBlank(message = "Email is required.")
    private String email;

    @NotBlank(message = "FirstName is required.")
    private String firstName;

    @NotBlank(message = "LastName is required.")
    private String lastName;

    private List<UUID> favouriteRecipes;
}
