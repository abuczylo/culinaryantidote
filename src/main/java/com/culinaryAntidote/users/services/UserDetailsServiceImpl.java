package com.culinaryAntidote.users.services;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    List<User> findAllByEmail(String email) {
        return userRepository.findAllByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<User> users = findAllByEmail(s);
        if(users.isEmpty()) {
            throw new ValidationException(GeneralErrorCode.invalidCredentialsErrorCode,
                    GeneralErrorMessage.invalidCredentialsErrorMessage);
        }
        User user = users.get(0);
        Set grantedAuthorities = getAuthorities(user);

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                grantedAuthorities);
    }

    private Set getAuthorities(User user) {
        return user.getRoles().stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName())).collect(Collectors.toSet());
    }
}
