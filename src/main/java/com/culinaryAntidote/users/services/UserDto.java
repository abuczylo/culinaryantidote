package com.culinaryAntidote.users.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserDto {

    private UUID id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private List<UUID> roles;
    private List<UUID> favouriteRecipes;
}
