package com.culinaryAntidote.users.services;

public class UserErrorCode {

    public static final String userNotFoundErrorCode = "UserNotFound";
    public static final String duplicatedUserErrorCode = "DuplicatedUser";
}
