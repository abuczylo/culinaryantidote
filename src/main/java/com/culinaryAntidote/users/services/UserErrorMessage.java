package com.culinaryAntidote.users.services;

public class UserErrorMessage {

    public static final String userNotFoundErrorMessage = "User not found.";
    public static final String duplicatedUserErrorMessage = "User already exist.";
}
