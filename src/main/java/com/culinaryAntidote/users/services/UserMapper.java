package com.culinaryAntidote.users.services;

import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.roles.persistance.Role;
import com.culinaryAntidote.users.persistance.User;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto mapToUserDto(User user);

    default List<UUID> mapToRoles(Set<Role> roles) {
        if (roles == null) {
            return new ArrayList<>();
        }
        return roles.stream().map(role -> role.getId()).collect(Collectors.toList());
    }

    default List<UUID> mapToFavouriteRecipes(Set<Recipe> recipes) {
        if (recipes == null) {
            return new ArrayList<>();
        }
        return recipes.stream().map(recipe -> recipe.getId()).collect(Collectors.toList());
    }
}
