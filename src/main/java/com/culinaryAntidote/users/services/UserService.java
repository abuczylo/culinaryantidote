package com.culinaryAntidote.users.services;

import com.culinaryAntidote.users.persistance.UserSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface UserService {

    Page<UserDto> findAll(Pageable pageable, UserSearchCriteria criteria);
    UserDto findById(UUID id);
    UserDto create(CreateUserDto createUserDto);
    UserDto update(UpdateUserDto updateUserDto);
    void delete(UUID id);
}
