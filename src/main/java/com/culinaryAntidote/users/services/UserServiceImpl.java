package com.culinaryAntidote.users.services;

import com.culinaryAntidote.common.exceptions.DuplicatedResourceException;
import com.culinaryAntidote.common.exceptions.ResourceNotFoundException;
import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.recipes.services.RecipeErrorCode;
import com.culinaryAntidote.recipes.services.RecipeErrorMessage;
import com.culinaryAntidote.roles.persistance.DefaultRole;
import com.culinaryAntidote.roles.persistance.Role;
import com.culinaryAntidote.roles.persistance.RoleRepository;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import com.culinaryAntidote.users.persistance.UserSearchCriteria;
import com.culinaryAntidote.users.persistance.UserSearchSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private RecipeRepository recipeRepository;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;

    @Override
    public Page<UserDto> findAll(Pageable pageable, UserSearchCriteria criteria) {
        UserSearchSpecification specification = new UserSearchSpecification(criteria);
        Page<User> pagedRecipeCategories = userRepository.findAll(specification, pageable);
        return pagedRecipeCategories.map(User -> userMapper.mapToUserDto(User));
    }

    @Override
    public UserDto findById(UUID id) {
        User user =
                userRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(UserErrorCode.userNotFoundErrorCode,
                                UserErrorMessage.userNotFoundErrorMessage));
        return userMapper.mapToUserDto(user);
    }

    @Override
    public UserDto create(CreateUserDto createUserDto) {
        if(userRepository.existsByEmail(createUserDto.getEmail())) {
            throw new DuplicatedResourceException(UserErrorCode.duplicatedUserErrorCode,
                    UserErrorMessage.duplicatedUserErrorMessage);
        }

        Role userRole = roleRepository.findByName(DefaultRole.USER.getName());
        User user = User.builder()
                .email(createUserDto.getEmail())
                .password(passwordEncoder.encode(createUserDto.getPassword()))
                .firstName(createUserDto.getFirstName())
                .lastName(createUserDto.getLastName())
                .roles(new HashSet<>(Arrays.asList(userRole)))
                .build();
        user = userRepository.save(user);
        return userMapper.mapToUserDto(user);
    }

    @Override
    public UserDto update(UpdateUserDto updateUserDto) {
        User user =
                userRepository.findById(updateUserDto.getId())
                        .orElseThrow(() -> new ResourceNotFoundException(UserErrorCode.userNotFoundErrorCode,
                                UserErrorMessage.userNotFoundErrorMessage));

        if(!updateUserDto.getEmail().equals(user.getEmail())) {
            if(userRepository.existsByEmail(updateUserDto.getEmail())) {
                throw new DuplicatedResourceException(UserErrorCode.duplicatedUserErrorCode,
                        UserErrorMessage.duplicatedUserErrorMessage);
            }
            user.setEmail(updateUserDto.getEmail());
        }

        Set<Recipe> favouriteRecipes = getRecipes(updateUserDto.getFavouriteRecipes());
        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setFavouriteRecipes(favouriteRecipes);
        user = userRepository.save(user);
        return userMapper.mapToUserDto(user);
    }

    @Override
    public void delete(UUID id) {
        User User =
                userRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(UserErrorCode.userNotFoundErrorCode,
                                UserErrorMessage.userNotFoundErrorMessage));
        userRepository.delete(User);
    }

    private Set<Recipe> getRecipes(List<UUID> recipeIds) {
        Set<Recipe> recipes = new HashSet<>();

        for (UUID recipeId : recipeIds) {
            Recipe recipe = recipeRepository.findById(recipeId)
                    .orElseThrow(() -> new ValidationException(RecipeErrorCode.recipeNotFoundErrorCode,
                            RecipeErrorMessage.recipeNotFoundErrorMessage));
            recipes.add(recipe);
        }

        return recipes;
    }
}
