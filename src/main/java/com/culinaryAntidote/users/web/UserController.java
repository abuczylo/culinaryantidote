package com.culinaryAntidote.users.web;

import com.culinaryAntidote.common.exceptions.ValidationException;
import com.culinaryAntidote.common.models.constants.GeneralErrorCode;
import com.culinaryAntidote.common.models.constants.GeneralErrorMessage;
import com.culinaryAntidote.users.persistance.UserSearchCriteria;
import com.culinaryAntidote.users.services.CreateUserDto;
import com.culinaryAntidote.users.services.UpdateUserDto;
import com.culinaryAntidote.users.services.UserDto;
import com.culinaryAntidote.users.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    @PreAuthorize("hasRole('Admin')")
    public Page<UserDto> getRecipeCategories(Pageable pageable, UserSearchCriteria criteria) {
        return userService.findAll(pageable, criteria);
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userResourceOwnerCheck.check(#id, authentication.getName())")
    public UserDto getUser(@PathVariable("id") UUID id) {
        return userService.findById(id);
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody CreateUserDto createUserDto,
                                              UriComponentsBuilder uriComponentsBuilder) {
        UserDto UserDto =  userService.create(createUserDto);
        UriComponents uriComponents =
                uriComponentsBuilder.path("/users/{id}").buildAndExpand(UserDto.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(UserDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userResourceOwnerCheck.check(#id, authentication.getName())")
    public UserDto updateUser(@PathVariable("id") UUID id,
                              @Valid @RequestBody UpdateUserDto updateUserDto) {

        if(!id.equals(updateUserDto.getId())) {
            throw new ValidationException(GeneralErrorCode.idMismatchErrorCode,
                    GeneralErrorMessage.idMismatchErrorMessage);
        }
        return userService.update(updateUserDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('Admin') || @userResourceOwnerCheck.check(#id, authentication.getName())")
    public ResponseEntity deleteUser(@PathVariable("id") UUID id) {
        userService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
