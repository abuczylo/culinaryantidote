create table recipe_categories(
     id uuid primary key not null default uuid_generate_v4(),
     name varchar(100) unique not null,
     polish_name varchar(100) unique not null,
     parent_id uuid null
);

-- main dishes
insert into recipe_categories(id, name, polish_name, parent_id)
values ('5da5c8db-3324-4ec7-83c0-717940deda8d', 'Main-dishes', 'Dania główne', null);

insert into recipe_categories(id, name, polish_name, parent_id)
values ('adce2277-3c54-4863-aec4-65c55a3cab23', 'Vegetarian-dishes', 'Dania wegetariańskie', '5da5c8db-3324-4ec7-83c0-717940deda8d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Vegetable-dishes', 'Dania z warzywami', 'adce2277-3c54-4863-aec4-65c55a3cab23');

insert into recipe_categories(id, name,polish_name,parent_id)
values ('34ea9b28-e9c7-49a2-80c8-8e9e3022784a', 'Fishes', 'Ryby', 'adce2277-3c54-4863-aec4-65c55a3cab23');

insert into recipe_categories(name,polish_name,parent_id)
values ('Sushi', 'Sushi', '34ea9b28-e9c7-49a2-80c8-8e9e3022784a');

insert into recipe_categories(name,polish_name,parent_id)
values ('Seafoods', 'Owoce morza', '34ea9b28-e9c7-49a2-80c8-8e9e3022784a');

insert into recipe_categories(name,polish_name,parent_id)
values ('Noodle-dishes', 'Dania z makaronem', '5da5c8db-3324-4ec7-83c0-717940deda8d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Meat-dishes', 'Dania mięse', '5da5c8db-3324-4ec7-83c0-717940deda8d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Stew-dishes', 'Dania jednogarnkowe', '5da5c8db-3324-4ec7-83c0-717940deda8d');

insert into recipe_categories(id, name,polish_name,parent_id)
values ('bed7ad9c-2445-4b98-9bfe-73fff35c4acc', 'Flour-dishes', 'Dania mączne', '5da5c8db-3324-4ec7-83c0-717940deda8d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Dumplings', 'Pierogi', 'bed7ad9c-2445-4b98-9bfe-73fff35c4acc');

insert into recipe_categories(name,polish_name,parent_id)
values ('Pancakes', 'Naleśniki', 'bed7ad9c-2445-4b98-9bfe-73fff35c4acc');

-- soups--
insert into recipe_categories(id,name,polish_name,parent_id)
values ('adc713e0-8312-4494-b653-2fd13e7ab019','Soups', 'Zupy', null);

insert into recipe_categories(id,name,polish_name,parent_id)
values ('167401f7-a4a3-4cb7-a476-9c0d6214a804','Vegetarian-soup', 'Zupa wegetariańska', 'adc713e0-8312-4494-b653-2fd13e7ab019');

insert into recipe_categories(name,polish_name,parent_id)
values ('Lentil-soup', 'Zupa z soczewicy', '167401f7-a4a3-4cb7-a476-9c0d6214a804');

insert into recipe_categories(name,polish_name,parent_id)
values ('Chickpea-soup','Zupa z ciecierzycy', '167401f7-a4a3-4cb7-a476-9c0d6214a804');

insert into recipe_categories(name,polish_name,parent_id)
values ('Fish-soup', 'Zupa rybna', '167401f7-a4a3-4cb7-a476-9c0d6214a804');

insert into recipe_categories(id, name,polish_name,parent_id)
values ('442fa430-ad96-4529-a927-e044dff26223', 'Daily-soup', 'Codzienna', 'adc713e0-8312-4494-b653-2fd13e7ab019');

insert into recipe_categories(name,polish_name,parent_id)
values ('Cream-soup', 'Krem', '442fa430-ad96-4529-a927-e044dff26223');

insert into recipe_categories(name,polish_name,parent_id)
values ('Tomato-soup', 'Zupa pomidorowa', '442fa430-ad96-4529-a927-e044dff26223');

insert into recipe_categories(name,polish_name,parent_id)
values ('Sour-soup', 'Żurek', '442fa430-ad96-4529-a927-e044dff26223');

insert into recipe_categories(name,polish_name,parent_id)
values ('White-borscht-soup', 'Barszcza biały', '442fa430-ad96-4529-a927-e044dff26223');

insert into recipe_categories(name,polish_name,parent_id)
values ('Beetroot-soup', 'Barszcz czerwony', '442fa430-ad96-4529-a927-e044dff26223');

insert into recipe_categories(id,name,polish_name,parent_id)
values ('a4448783-aa7d-403e-b3d6-5871ae019b3c','Japanese-soups', 'Zupa japońska', 'adc713e0-8312-4494-b653-2fd13e7ab019');

insert into recipe_categories(name,polish_name,parent_id)
values ('Ramen', 'Ramen', 'a4448783-aa7d-403e-b3d6-5871ae019b3c');

-- salads
insert into recipe_categories(id,name,polish_name,parent_id)
values ('c4491f0e-0019-461e-8aec-5be3194fe82f','Salads', 'Sałatki', null);

insert into recipe_categories(id,name,polish_name,parent_id)
values ('3c27a65f-a114-4f8b-b5fb-9bc921563e21','Vegetarian-salads', 'Sałatki wegetariańskie', 'c4491f0e-0019-461e-8aec-5be3194fe82f');

insert into recipe_categories(name,polish_name,parent_id)
values ('Egg-salads', 'Sałatki z jajkiem', '3c27a65f-a114-4f8b-b5fb-9bc921563e21');

insert into recipe_categories(name,polish_name,parent_id)
values ('Vegetable-salads', 'Sałatki jarzynowe', '3c27a65f-a114-4f8b-b5fb-9bc921563e21');

insert into recipe_categories(name,polish_name,parent_id)
values ('Seafood-salads', 'Sałatki z owocami morza', '3c27a65f-a114-4f8b-b5fb-9bc921563e21');

insert into recipe_categories(name,polish_name,parent_id)
values ('Fruit-salads', 'Sałatki z owocami', '3c27a65f-a114-4f8b-b5fb-9bc921563e21');

insert into recipe_categories(name,polish_name,parent_id)
values ('Salads-for-work', 'Sałatki do pracy', 'c4491f0e-0019-461e-8aec-5be3194fe82f');

insert into recipe_categories(name,polish_name,parent_id)
values ('Fit-salads', 'Sałatki lekkie', 'c4491f0e-0019-461e-8aec-5be3194fe82f');

insert into recipe_categories(id,name,polish_name,parent_id)
values ('61c18f61-ddd8-48e8-8568-ba57a2097c20','Meat-salads', 'Sałatki mięsne', 'c4491f0e-0019-461e-8aec-5be3194fe82f');

insert into recipe_categories(name,polish_name,parent_id)
values ('Chicken-salads', 'Sałatki z kurczakiem', '61c18f61-ddd8-48e8-8568-ba57a2097c20');

--breakfast
insert into recipe_categories(id,name,polish_name,parent_id)
values ('5996943f-9c2e-4904-92af-0f099b0b5e54','Breakfasts', 'Śniadania', null);

insert into recipe_categories(id,name,polish_name,parent_id)
values ('299a3f7b-6d85-4800-9135-34f741512066','Sandwiches', 'Kanapki', '5996943f-9c2e-4904-92af-0f099b0b5e54');

insert into recipe_categories(name,polish_name,parent_id)
values ('Meat-sandwiches', 'Kanapki z mięsem', '299a3f7b-6d85-4800-9135-34f741512066');

insert into recipe_categories(name,polish_name,parent_id)
values ('Vegetarian-sandwiches', 'Kanapki wegetariańskie', '299a3f7b-6d85-4800-9135-34f741512066');

insert into recipe_categories(name,polish_name,parent_id)
values ('Breakfast-pancakes', 'Naleśniki śniadaniowe', '5996943f-9c2e-4904-92af-0f099b0b5e54');

insert into recipe_categories(name,polish_name,parent_id)
values ('Waffles', 'Gofry', '5996943f-9c2e-4904-92af-0f099b0b5e54');

insert into recipe_categories(name,polish_name,parent_id)
values ('Granola', 'Granola', '5996943f-9c2e-4904-92af-0f099b0b5e54');

insert into recipe_categories(name,polish_name,parent_id)
values ('Eggs', 'Jajka', '5996943f-9c2e-4904-92af-0f099b0b5e54');

-- fast food
insert into recipe_categories(id,name,polish_name,parent_id)
values ('d92cdd57-3bab-43ea-89bc-01cc5c071807','Fast-food', 'Fast food', null);

insert into recipe_categories(id,name,polish_name,parent_id)
values ('d97edfde-f418-4b0e-b203-7fa38e03fcfe','Vegetarian-fast-food', 'Fast food wegetariański', 'd92cdd57-3bab-43ea-89bc-01cc5c071807');

insert into recipe_categories(name,polish_name,parent_id)
values ('Pizza', 'Pizza', 'd92cdd57-3bab-43ea-89bc-01cc5c071807');

insert into recipe_categories(name,polish_name,parent_id)
values ('Hot-dogs', 'Hot dogi', 'd92cdd57-3bab-43ea-89bc-01cc5c071807');

insert into recipe_categories(name,polish_name,parent_id)
values ('Burgers', 'Burgery', 'd92cdd57-3bab-43ea-89bc-01cc5c071807');

insert into recipe_categories(name,polish_name,parent_id)
values ('Chips', 'Frytki', 'd92cdd57-3bab-43ea-89bc-01cc5c071807');

-- desserts
insert into recipe_categories(id,name,polish_name,parent_id)
values ('f25b9cc1-d94b-4019-b43d-d4a2b80f4d47','Desserts', 'Desery', null);

insert into recipe_categories(id,name,polish_name,parent_id)
values ('a427af3c-29cd-412b-9957-882cdebf5831','Sweet-desserts', 'Desery na słodko', 'f25b9cc1-d94b-4019-b43d-d4a2b80f4d47');

insert into recipe_categories(id,name,polish_name,parent_id)
values ('5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d','Cakes', 'Cista', 'a427af3c-29cd-412b-9957-882cdebf5831');

insert into recipe_categories(name,polish_name,parent_id)
values ('Sponge-cakes', 'Biszkopty', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Cheese-cakes', 'Serniki', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Tart', 'Tarta', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(id,name,polish_name,parent_id)
values ('be72a955-d9d8-432c-9013-212abfa9e499','Fruit-cakes', 'Ciasta z owocami', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Apple-pies', 'Jabłeczniki', 'be72a955-d9d8-432c-9013-212abfa9e499');

insert into recipe_categories(name,polish_name,parent_id)
values ('Plum-cakes', 'Ciasta ze śliwkami', 'be72a955-d9d8-432c-9013-212abfa9e499');

insert into recipe_categories(name,polish_name,parent_id)
values ('Strawberry-cakes', 'Ciasta z Truskawkami', 'be72a955-d9d8-432c-9013-212abfa9e499');

insert into recipe_categories(name,polish_name,parent_id)
values ('Chocolate-cakes', 'Ciasta czekoladowe', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(id,name,polish_name,parent_id)
values ('493a20eb-9bf5-4aa7-b664-8fc08e352da4','Vegetable-cakes', 'Warzywne ciasta', '5e2fd98c-a5bb-4069-bc2b-d4dc4855d24d');

insert into recipe_categories(name,polish_name,parent_id)
values ('Carrot-cakes', 'Ciasta marchewkowe', '493a20eb-9bf5-4aa7-b664-8fc08e352da4');

insert into recipe_categories(name,polish_name,parent_id)
values ('Spinach-cakes', 'Ciasta ze szpinakiem', '493a20eb-9bf5-4aa7-b664-8fc08e352da4');

insert into recipe_categories(name,polish_name,parent_id)
values ('Pumpkin-pies', 'Ciasta z dynia', '493a20eb-9bf5-4aa7-b664-8fc08e352da4');