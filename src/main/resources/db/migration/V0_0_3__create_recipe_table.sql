create table recipes(
     id uuid primary key not null default uuid_generate_v4(),
     name varchar(100) not null,
     portions_amount int not null,
     difficulty_level varchar(100) not null,
     preparation_time int not null,
     ingredients text not null,
     description text not null,
     creation_date timestamptz not null default now(),
     update_date timestamptz null,
     average_rating NUMERIC(3, 2) null
);
