create table recipe_recipe_categories(
     recipe_id uuid not null references recipes(id),
     recipe_category_id uuid not null references recipe_categories(id),
     CONSTRAINT recipe_recipe_categories_pk PRIMARY KEY (recipe_id, recipe_category_id)
);