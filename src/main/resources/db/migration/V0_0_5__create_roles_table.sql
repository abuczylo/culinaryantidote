create table roles(
     id uuid primary key not null default uuid_generate_v4(),
     name varchar(100) not null
);

insert into roles(name) values ('User');
insert into roles(name) values ('Admin');
