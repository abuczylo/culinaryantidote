create table users(
     id uuid primary key not null default uuid_generate_v4(),
     email varchar(255) not null,
     password varchar(255) not null,
     first_name varchar(100) not null,
     last_name varchar(100) not null
);
