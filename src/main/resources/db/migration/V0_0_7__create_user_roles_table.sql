create table user_roles(
     user_id uuid not null references users(id),
     role_id uuid not null references roles(id),
     CONSTRAINT user_roles_pk PRIMARY KEY (user_id, role_id)
);