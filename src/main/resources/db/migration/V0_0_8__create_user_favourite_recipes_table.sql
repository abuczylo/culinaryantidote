create table user_favourite_recipes(
     user_id uuid not null references users(id),
     recipe_id uuid not null references recipes(id),
     CONSTRAINT user_favourite_recipes_pk PRIMARY KEY (user_id, recipe_id)
);