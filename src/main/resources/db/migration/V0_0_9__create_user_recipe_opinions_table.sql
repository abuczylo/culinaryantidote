create table user_recipe_opinions(
     id uuid primary key not null default uuid_generate_v4(),
     user_id uuid not null references users(id),
     recipe_id uuid not null references recipes(id),
     rating integer not null check (rating > 0 and rating < 6),
     comment varchar(255) not null
);