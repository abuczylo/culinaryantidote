package com.culinaryAntidote.recipes;

import com.culinaryAntidote.recipeCategories.persistence.RecipeCategory;
import com.culinaryAntidote.recipeCategories.persistence.RecipeCategoryRepository;
import com.culinaryAntidote.recipes.persistance.DifficultyLevel;
import com.culinaryAntidote.recipes.persistance.Recipe;
import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.recipes.persistance.RecipeSearchCriteria;
import com.culinaryAntidote.recipes.services.RecipeDto;
import com.culinaryAntidote.recipes.services.RecipeMapper;
import com.culinaryAntidote.recipes.services.RecipeServiceImpl;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecipeServiceImplTest {

    @InjectMocks
    private RecipeServiceImpl service;

    @Mock
    private RecipeRepository recipeRepositoryMock;

    @Mock
    private RecipeCategoryRepository recipeCategoryRepositoryMock;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private RecipeMapper recipeMapperMock;

    @Test
    public void findAll_should_return_Page_with_RecipeDto() {
        Pageable pageable = PageRequest.of(0,100);
        RecipeSearchCriteria recipeSearchCriteria = new RecipeSearchCriteria();
        List<RecipeCategory> recipeCategoryList = Arrays.asList(RecipeCategory.builder()
                .id(UUID.randomUUID())
                .name("main")
                .polishName("glowna")
                .build());
        Set<RecipeCategory> recipeCategories = new HashSet(recipeCategoryList);
        User user = User.builder()
                .id(UUID.randomUUID())
                .email("email@email.com")
                .firstName("fistName")
                .lastName("lastName")
                .password("password1234")
                .build();
        Recipe recipe = Recipe.builder()
                .id(UUID.randomUUID())
                .name("name")
                .description("description")
                .difficultyLevel(DifficultyLevel.EASY)
                .portionsAmount(4)
                .preparationTime(30)
                .ingredients("water")
                .averageRating(4.5)
                .creationDate(OffsetDateTime.now().minusDays(2))
                .updateDate(OffsetDateTime.now())
                .user(user)
                .recipeCategories(recipeCategories)
                .build();
        List<Recipe> recipes = Arrays.asList(recipe);
        Page<Recipe> recipePage = new PageImpl<>(recipes);
        RecipeDto recipeDto = RecipeDto.builder()
                .id(recipe.getId())
                .name(recipe.getName())
                .description(recipe.getDescription())
                .difficultyLevel(recipe.getDifficultyLevel())
                .portionsAmount(recipe.getPortionsAmount())
                .preparationTime(recipe.getPreparationTime())
                .ingredients(recipe.getIngredients())
                .averageRating(recipe.getAverageRating())
                .creationDate(recipe.getCreationDate())
                .updateDate(recipe.getUpdateDate())
                .userId(recipe.getUser().getId())
                .recipeCategories(recipe.getRecipeCategories().stream().map(rc -> rc.getId()).collect(Collectors.toList()))
                .build();
        List<RecipeDto> recipeDtos = Arrays.asList(recipeDto);
        Page<RecipeDto> expectedResult = new PageImpl<>(recipeDtos);
        when(recipeRepositoryMock.findAll(any(Specification.class), any(Pageable.class))).thenReturn(recipePage);
        when(recipeMapperMock.mapToRecipeDto(any(Recipe.class))).thenReturn(recipeDto);

        assertThat(service.findAll(pageable, recipeSearchCriteria)).isEqualTo(expectedResult);
    }
}
