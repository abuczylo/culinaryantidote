package com.culinaryAntidote.users;

import com.culinaryAntidote.recipes.persistance.RecipeRepository;
import com.culinaryAntidote.roles.persistance.RoleRepository;
import com.culinaryAntidote.users.persistance.User;
import com.culinaryAntidote.users.persistance.UserRepository;
import com.culinaryAntidote.users.services.UserDto;
import com.culinaryAntidote.users.services.UserMapper;
import com.culinaryAntidote.users.services.UserService;
import com.culinaryAntidote.users.services.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {

    private UserRepository userRepositoryMock;
    private RoleRepository roleRepository;
    private RecipeRepository recipeRepository;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;
    private UserService service;

    @Before
    public void before() {
        this.userRepositoryMock = mock(UserRepository.class);
        this.roleRepository = mock(RoleRepository.class);
        this.recipeRepository = mock(RecipeRepository.class);
        this.userMapper = mock(UserMapper.class);
        this.passwordEncoder = mock(PasswordEncoder.class);
        this.service = new UserServiceImpl(this.userRepositoryMock, this.roleRepository, this.recipeRepository,
                this.userMapper, this.passwordEncoder);
    }

    @Test
    public void findById_should_return_UserDto() {
        UUID userId = UUID.randomUUID();
        User user = User.builder()
                .id(userId)
                .email("email@email.com")
                .firstName("fistName")
                .lastName("lastName")
                .password("password1234")
                .build();
        Optional<User> optionalUser = Optional.of(user);
        UserDto userDto = UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .build();
        when(userRepositoryMock.findById(any(UUID.class))).thenReturn(optionalUser);
        when(userMapper.mapToUserDto(any(User.class))).thenReturn(userDto);

        assertThat(service.findById(userId)).isEqualTo(userDto);
    }
}
